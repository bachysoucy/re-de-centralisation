2014-10 ESADHaR workshop

#re-de-centralisation

## AXES

### reseaux
- reseaux mesh
	- telephone mobile : hongkong, serval
- http://qaul.net/text_en.html

### accès internet
fournisseurs d'accès indépendant
- http://www.fdn.fr/

### échange de données
torrent, bittorrent

### hébergement
mail, stockage
- http://owncloud.org

##### hébergement associatif
- http://www.lautre.net/
- http://www.fdn.fr/

##### auto-hébergement
- http://yunohost.org

### resaux sociaux
- jappix
- dispora
- http://twister.net.co/


## links
- http://core.servus.at/
- http://qaul.net/text_en.html
- http://www.ffdn.org/